module Engine.Font where

import Prelude hiding (lookup, forM , forM_ , mapM , mapM_ , msum , sequence , sequence_)
import Data.Map (lookup, fromList, Map)
import Data.Maybe (fromMaybe)
import Engine.Tile
import Engine.Image
import Engine.Util

data Font = Font (Map Char Tile) Tile deriving (Show)


renderStr :: Font -> Int -> [Char] -> Int -> Int -> [BlitParams]
renderStr (Font m t) scale str x y =
    let tileList = fmap (fromMaybe t . flip lookup m) str
        offsetList = zipWith (*) (fmap (fst . getTileSize) tileList) [0, 1..]
        sizeList = fmap getTileSize tileList
    in zipWith3 (\t offset (w, h) -> renderTile t $ Just (toRect (x + offset*scale) y (w*scale) (h*scale))) tileList offsetList sizeList

