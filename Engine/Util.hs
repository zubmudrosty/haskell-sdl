module Engine.Util (toRect, Rect(..)) where

import Graphics.UI.SDL (Rect(..))

toRect :: (Integral a) => a -> a -> a -> a -> Rect
toRect a b c d = Rect (fromIntegral a) (fromIntegral b) (fromIntegral c) (fromIntegral d)

