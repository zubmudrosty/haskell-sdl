module Engine.Event (pollEventList, isQuitEvent) where

import Prelude hiding (forM , forM_ , mapM , mapM_ , msum , sequence , sequence_)
import qualified Graphics.UI.SDL as SDL
import Foreign.Ptr (Ptr)
import Foreign.Marshal.Alloc (alloca)
import Foreign.Storable (peek)

pollEventList' :: [SDL.Event] -> Ptr SDL.Event -> IO [SDL.Event]
pollEventList' es eventPtr = do
    pending <- SDL.pollEvent eventPtr
    event <- peek eventPtr
    case pending of
        0 -> return es
        1 -> pollEventList' (event:es) eventPtr

pollEventList :: IO [SDL.Event]
pollEventList = alloca (pollEventList' [])

isQuitEvent :: SDL.Event -> Bool
isQuitEvent (SDL.QuitEvent _ _) = True
isQuitEvent _ = False

