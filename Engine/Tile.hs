module Engine.Tile where

import Engine.Image
import Engine.Util

data Tileset = Tileset 
    { tileName :: String
    , tileWidth :: Int
    , tileHeight :: Int
    } deriving (Show)

data Tile = Tile
    { getTileset :: Tileset
    , getCoord :: (Int, Int)
    } deriving (Show)

renderTile :: Tile -> Maybe Rect -> BlitParams
renderTile (Tile (Tileset name w h) (x, y)) =
    let tileRect = toRect (x*w) (y*h) w h
    in BlitParams name (Just tileRect)

getTileSize :: Tile -> (Int, Int)
getTileSize (Tile (Tileset _ w h) _) = (w, h)

