module Engine.Image
( Texture (..)
, TextureMap
, Blitter
, BlitParams(..)
, loadImg
, loadImgMap
, draw
) where

import Prelude hiding (forM , forM_ , mapM , mapM_ , msum , sequence , sequence_)
import qualified Graphics.UI.SDL as SDL
import qualified Data.Map as Map
import qualified Data.Set as Set
import Data.Traversable (sequence)
import Data.Foldable (mapM_)
import Foreign.C.String (newCString)
import Foreign.Marshal.Utils as MU
import Control.Exception (bracket, bracket_)
import Foreign.Storable (peek)

import Debug.Trace

import Engine.Util

data Texture = Texture SDL.Texture Int Int
loadImg :: SDL.Renderer -> String -> IO Texture
loadImg r name =
    bracket
        (SDL.loadBMP =<< newCString name)
        SDL.freeSurface
        (\s -> do
            t <- SDL.createTextureFromSurface r s
            s' <- peek s
            return $ Texture t (fromIntegral $ SDL.surfaceW s') (fromIntegral $ SDL.surfaceH s'))

type TextureMap = Map.Map String Texture
loadImgMap :: SDL.Renderer -> [String] -> IO TextureMap
loadImgMap r = sequence . Map.fromSet (loadImg r) . Set.fromList

draw' :: SDL.Renderer -> Texture -> Maybe SDL.Rect -> Maybe SDL.Rect -> IO ()
draw' r (Texture t _ _) srcRect dstRect = do
    src <- MU.maybeNew MU.new srcRect
    dst <- MU.maybeNew MU.new dstRect
    SDL.renderCopy r t src dst
    return ()

data BlitParams = BlitParams String (Maybe SDL.Rect) (Maybe SDL.Rect) deriving (Show)
type Blitter = [BlitParams] -> IO ()
draw :: SDL.Renderer -> TextureMap -> Blitter
draw r tMap paramList =
    let blt (BlitParams name src dst) = draw' r (tMap Map.! name) src dst
    in bracket_
        (SDL.renderClear r)
        (SDL.renderPresent r)
        (mapM_ blt paramList)

