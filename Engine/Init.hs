module Engine.Init where

import Prelude hiding (forM , forM_ , mapM , mapM_ , msum , sequence , sequence_)
import qualified Graphics.UI.SDL as SDL
import Foreign.C.String (newCString)
import Data.Traversable (mapM)
import Control.Exception (bracket, bracket_)
import Data.Bits ((.|.))

import Engine.Image

data WindowParams = WindowParams
    { title :: String
    , width :: Int
    , height :: Int
    } deriving (Show)

createStdWindow :: WindowParams -> IO SDL.Window
createStdWindow params = do
    t <- newCString (title params)
    SDL.createWindow
        t
        SDL.SDL_WINDOWPOS_UNDEFINED
        SDL.SDL_WINDOWPOS_UNDEFINED
        (fromIntegral $ width params)
        (fromIntegral $ height params)
        SDL.SDL_WINDOW_OPENGL

withInit :: IO () -> IO ()
withInit =
    bracket_
        (SDL.init SDL.SDL_INIT_EVERYTHING)
        SDL.quit

withWindow :: WindowParams -> (SDL.Window -> IO ()) -> IO ()
withWindow params f = withInit $ bracket (createStdWindow params) SDL.destroyWindow f
    
withRender :: WindowParams -> (SDL.Renderer -> IO ()) -> IO ()
withRender params f = withWindow params $ \w ->
    let setColor r = SDL.setRenderDrawColor r 0 0 0 255
        flags = SDL.SDL_RENDERER_ACCELERATED .|. SDL.SDL_RENDERER_PRESENTVSYNC
    in bracket
        (SDL.createRenderer w (-1) flags)
        SDL.destroyRenderer
        (\r -> setColor r >> f r)

withImages :: [String] -> SDL.Renderer -> (TextureMap -> IO ()) -> IO ()
withImages imgNames r =
    bracket
        (loadImgMap r imgNames)
        (mapM (SDL.destroyTexture . unwrapTexture))
    where unwrapTexture (Texture t _ _) = t

