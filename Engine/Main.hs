module Engine.Main where

import Prelude hiding (forM , forM_ , mapM , mapM_ , msum , sequence , sequence_)
import qualified Graphics.UI.SDL as SDL
import Control.Monad (unless)
import Data.Time.Clock.POSIX (getPOSIXTime, POSIXTime)
import Data.Map (Map)

import Engine.Init
import Engine.Event
import Engine.Image
import Engine.Font

data GameSettings w a = GameSettings
    { windowParams :: WindowParams
    , textures :: [String]
    , initWorld :: w
    , updateCallback :: a -> [SDL.Event] -> POSIXTime -> w -> IO w
    , renderCallback :: TextureMap -> w -> [BlitParams]
    , loadCallback :: SDL.Renderer -> IO a
    , unloadCallback :: a -> IO ()
    }

stdGameSettings = GameSettings
    { textures = []
    , initWorld = ()
    , updateCallback = \_ _ _ -> return
    , renderCallback = \_ _ -> []
    , loadCallback = \_ -> return ()
    , unloadCallback = \_ -> return ()

    , windowParams = undefined
    }

loop blit update render world prevTime =
    let loop' = loop blit update render
    in do
        es <- pollEventList
        curTime <- getPOSIXTime

        world' <- update es (curTime - prevTime) $! world
        blit $ render world'

        unless (any isQuitEvent es) (loop' world' curTime)


runGame settings = withRender (windowParams settings) $ \r ->
    withImages (textures settings) r $ \tMap -> do
        assets <- loadCallback settings $ r
        time <- getPOSIXTime
        loop
            (draw r tMap)
            (updateCallback settings $ assets)
            (renderCallback settings $ tMap)
            (initWorld settings)
            time
        unloadCallback settings $ assets

