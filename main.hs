import qualified Graphics.UI.SDL as SDL

import Engine.Main
import Engine.Image
import Engine.Tile
import Engine.Font
import Engine.Util
import Engine.Init

import Data.Map ((!), fromList, Map)
import Data.List.Split (chunksOf)
import Data.List (transpose)

import Foreign.C.String (newCString)
import Foreign (nullPtr)

import Debug.Trace

fontLetters =
    [ " !\"#$%&'()*+,-./01"
    , "23456789:;<=>?@ABC"
    , "DEFGHIJKLMNOPQRSTU"
    , "VWXYZ[\\]^_`abcdefg"
    , "hijklmnopqrstuvwxy"
    , "z{|}~"
    ]

enumWith f = zipWith f [0..]
stdFontTileset = Tileset "font.bmp" 7 9
fm = fmap (Tile stdFontTileset) . fromList . concat . enumWith (\i -> enumWith (\j c -> (c, (j, i))) ) $ fontLetters

stdFont = Font fm (fm ! '?')
rs = renderStr stdFont 4 "Hello world" 16 16

someImages =
    [ BlitParams "background.bmp" Nothing Nothing
    , BlitParams "hello.bmp" Nothing (Just $ Rect 0 0 64 64)
    , BlitParams "font.bmp" Nothing (Just $ Rect 64 0 64 64)
    ]
render tm w = someImages ++ rs

update assets es dt w = do
    return $ traceShow es w

main :: IO ()
main = runGame stdGameSettings
    { windowParams = WindowParams "ololo" 640 480
    , textures = ["background.bmp", "hello.bmp", "font.bmp"]
    , updateCallback = update
    , renderCallback = render
    }

